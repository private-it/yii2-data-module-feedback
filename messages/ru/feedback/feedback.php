<?php
return [
    'label.id' => '#',
    'label.name' => 'Имя',
    'label.email' => 'Почта',
    'label.phone' => 'Телефон',
    'label.message' => 'Сообщение',
    'label.status' => 'Статус',
    'label.created_at' => 'Дата создания',
    'label.updated_at' => 'Дата обновления',

    'hint.id' => "\r",
    'hint.name' => "\r",
    'hint.email' => "\r",
    'hint.phone' => "\r",
    'hint.message' => "\r",
    'hint.status' => "\r",
    'hint.created_at' => "\r",
    'hint.updated_at' => "\r",

    'placeholder.id' => "\r",
    'placeholder.name' => "\r",
    'placeholder.email' => "\r",
    'placeholder.phone' => "\r",
    'placeholder.message' => "\r",
    'placeholder.status' => "\r",
    'placeholder.created_at' => "\r",
    'placeholder.updated_at' => "\r",
];
