<?php
/**
 * Created by ERDConverter
 */

use yii\db\Schema;
use yii\db\Migration;

/**
 * m160413_213451_001_create_feedback
 *
 */
class m160413_213451_001_create_feedback extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(\PrivateIT\modules\feedback\models\Feedback::tableName(), [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(""),
            'email' => $this->string()->defaultValue(""),
            'phone' => $this->string()->defaultValue(""),
            'message' => $this->text()->defaultValue(""),
            'status' => $this->integer()->defaultValue(0),
            'created_at' => $this->timestamp()->defaultValue(null),
            'updated_at' => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(\PrivateIT\modules\feedback\models\Feedback::tableName());
    }
}