<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\feedback\models\query;

use PrivateIT\modules\feedback\models\Feedback;

/**
 * FeedbackActiveQuery
 *
 */
class FeedbackActiveQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return Feedback[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Feedback|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /*
    public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }
    */
}
