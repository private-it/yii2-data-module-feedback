<?php
/**
 * Created by ERDConverter
 */
namespace PrivateIT\modules\feedback\models;

use PrivateIT\modules\feedback\FeedbackModule;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Feedback
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 */
class Feedback extends ActiveRecord
{
    const STATUS_ARCHIVED = -1;
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * Get object statuses
     *
     * @return array
     */
    static function getStatuses()
    {
        return [
            static::STATUS_ARCHIVED => Yii::t('feedback/feedback', 'const.status.archived'),
            static::STATUS_DELETED => Yii::t('feedback/feedback', 'const.status.deleted'),
            static::STATUS_ACTIVE => Yii::t('feedback/feedback', 'const.status.active'),
        ];
    }

    /**
     * @inheritdoc
     * @return query\FeedbackActiveQuery the newly created [[ActiveQuery]] instance.
     */
    public static function find()
    {
        return parent::find();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return FeedbackModule::tableName(__CLASS__);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TimestampBehavior::className(),
            'value' => new Expression('NOW()'),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('feedback/feedback', 'label.id'),
            'name' => Yii::t('feedback/feedback', 'label.name'),
            'email' => Yii::t('feedback/feedback', 'label.email'),
            'phone' => Yii::t('feedback/feedback', 'label.phone'),
            'message' => Yii::t('feedback/feedback', 'label.message'),
            'status' => Yii::t('feedback/feedback', 'label.status'),
            'created_at' => Yii::t('feedback/feedback', 'label.created_at'),
            'updated_at' => Yii::t('feedback/feedback', 'label.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('feedback/feedback', 'hint.id'),
            'name' => Yii::t('feedback/feedback', 'hint.name'),
            'email' => Yii::t('feedback/feedback', 'hint.email'),
            'phone' => Yii::t('feedback/feedback', 'hint.phone'),
            'message' => Yii::t('feedback/feedback', 'hint.message'),
            'status' => Yii::t('feedback/feedback', 'hint.status'),
            'created_at' => Yii::t('feedback/feedback', 'hint.created_at'),
            'updated_at' => Yii::t('feedback/feedback', 'hint.updated_at'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributePlaceholders()
    {
        return [
            'id' => Yii::t('feedback/feedback', 'placeholder.id'),
            'name' => Yii::t('feedback/feedback', 'placeholder.name'),
            'email' => Yii::t('feedback/feedback', 'placeholder.email'),
            'phone' => Yii::t('feedback/feedback', 'placeholder.phone'),
            'message' => Yii::t('feedback/feedback', 'placeholder.message'),
            'status' => Yii::t('feedback/feedback', 'placeholder.status'),
            'created_at' => Yii::t('feedback/feedback', 'placeholder.created_at'),
            'updated_at' => Yii::t('feedback/feedback', 'placeholder.updated_at'),
        ];
    }

    /**
     * Get value from Id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value to Id
     *
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * Get value from Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value to Name
     *
     * @param $value
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * Get value from Email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set value to Email
     *
     * @param $value
     * @return $this
     */
    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    /**
     * Get value from Phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set value to Phone
     *
     * @param $value
     * @return $this
     */
    public function setPhone($value)
    {
        $this->phone = $value;
        return $this;
    }

    /**
     * Get value from Message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set value to Message
     *
     * @param $value
     * @return $this
     */
    public function setMessage($value)
    {
        $this->message = $value;
        return $this;
    }

    /**
     * Get value from Status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set value to Status
     *
     * @param $value
     * @return $this
     */
    public function setStatus($value)
    {
        $this->status = $value;
        return $this;
    }

    /**
     * Get value from CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set value to CreatedAt
     *
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->created_at = $value;
        return $this;
    }

    /**
     * Get value from UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set value to UpdatedAt
     *
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updated_at = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function send()
    {
        $this->setStatus(static::STATUS_ACTIVE);
        return $this->save(false);
    }
}
